options:
  parameters:
    author: Manolis Surligas (surligas@gmail.com)
    category: '[GRC Hier Blocks]'
    cmake_opt: ''
    comment: ''
    copyright: ''
    description: Qubik Transceiver
    gen_cmake: 'On'
    gen_linking: dynamic
    generate_options: qt_gui
    hier_block_src_path: '.:'
    id: qubik_transceiver
    max_nouts: '0'
    output_language: python
    placement: (0,0)
    qt_qss_theme: ''
    realtime_scheduling: ''
    run: 'True'
    run_command: '{python} -u {filename}'
    run_options: run
    sizing_mode: fixed
    thread_safe_setters: ''
    title: qubik_transceiver
    window_size: 2*1080,1080
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [8, 4.0]
    rotation: 0
    state: enabled

blocks:
- name: audio_samp_rate
  id: variable
  parameters:
    comment: ''
    value: '48000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1440, 12.0]
    rotation: 0
    state: enabled
- name: decimation
  id: variable
  parameters:
    comment: ''
    value: max(4,satnogs.find_decimation(baudrate, 2, audio_samp_rate))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1248, 12.0]
    rotation: 0
    state: true
- name: deviation
  id: variable
  parameters:
    comment: The FSK frequency deviation
    value: 4.8e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [720, 972.0]
    rotation: 0
    state: enabled
- name: gain_rx
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: RX Gain
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: '0'
    step: '1'
    stop: '60'
    value: '30'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1018, 13]
    rotation: 0
    state: true
- name: gain_tx
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: TX Gain
    min_len: '100'
    orient: Qt.Horizontal
    rangeType: float
    start: '0'
    step: '1'
    stop: '60'
    value: '50'
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1184, 999]
    rotation: 0
    state: true
- name: gaussian_taps
  id: variable
  parameters:
    comment: ''
    value: filter.firdes.gaussian(1.0, sps_tx, 1.0, 4*sps_tx)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 1340.0]
    rotation: 0
    state: enabled
- name: interp_taps
  id: variable
  parameters:
    comment: ''
    value: numpy.convolve(numpy.array(gaussian_taps), numpy.array(sq_wave))
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 1468.0]
    rotation: 0
    state: enabled
- name: modulation_index
  id: variable
  parameters:
    comment: ''
    value: deviation / (baudrate / 2.0)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 1276.0]
    rotation: 0
    state: enabled
- name: rx_freq
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: RX Freq
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: rx_freq-10e3
    step: 1e3
    stop: rx_freq+10e3
    value: 435.240e6
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1582, 17]
    rotation: 0
    state: true
- name: sps_tx
  id: variable
  parameters:
    comment: Samples per symbol
    value: '32'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [608, 972.0]
    rotation: 0
    state: true
- name: sq_wave
  id: variable
  parameters:
    comment: ''
    value: (1.0, ) * sps_tx
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 1404.0]
    rotation: 0
    state: enabled
- name: tx_freq
  id: variable_qtgui_range
  parameters:
    comment: ''
    gui_hint: ''
    label: TX Freq
    min_len: '200'
    orient: Qt.Horizontal
    rangeType: float
    start: tx_freq-10e3
    step: 1e3
    stop: tx_freq+10e3
    value: 435.240e6
    widget: counter_slider
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1438, 997]
    rotation: 0
    state: true
- name: variable_ieee802_15_4_encoder_0
  id: variable_ieee802_15_4_encoder
  parameters:
    comment: 'Some notes about the uplink:

      * The payload is RS encoded therefore

      the CRC should be computed prior

      to the this encoder'
    crc: satnogs.crc.CRC_NONE
    preamble: '0x33'
    preamble_len: '8'
    sync_word: '[0x1A, 0xCF, 0xFC, 0x1D]'
    var_len: 'False'
    whitening: satnogs.whitening_sptr(None)
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [880, 996.0]
    rotation: 0
    state: true
- name: variable_ieee802_15_4_variant_decoder_0
  id: variable_ieee802_15_4_variant_decoder
  parameters:
    comment: ''
    crc: satnogs.crc.CRC32_C
    frame_len: '128'
    preamble: '[0x33]*4'
    preamble_thrsh: '5'
    sync_thrsh: '3'
    sync_word: '[0x5e, 0x7a, 0x0d, 0xce]'
    var_len: 'False'
    whitening: variable_whitening_ccsds_descrambler
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1491, 174]
    rotation: 0
    state: true
- name: variable_rs_encoder_0
  id: variable_rs_encoder
  parameters:
    comment: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1672, 1012.0]
    rotation: 0
    state: true
- name: variable_whitening_ccsds_descrambler
  id: variable_whitening_ccsds
  parameters:
    comment: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1919, 301]
    rotation: 0
    state: true
- name: analog_frequency_modulator_fc_0
  id: analog_frequency_modulator_fc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    sensitivity: (math.pi*modulation_index) / sps_tx
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1928, 1276.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.2'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1280, 524.0]
    rotation: 0
    state: enabled
- name: analog_quadrature_demod_cf_0_0_0_0
  id: analog_quadrature_demod_cf
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain: '1.0'
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [496, 524.0]
    rotation: 0
    state: enabled
- name: antenna_rx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [906, 13]
    rotation: 0
    state: enabled
- name: antenna_tx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1064, 996]
    rotation: 0
    state: enabled
- name: baudrate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: '9600.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1352, 12.0]
    rotation: 0
    state: enabled
- name: blocks_delay_0
  id: blocks_delay
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    delay: 1024//2
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [536, 452.0]
    rotation: 0
    state: enabled
- name: blocks_moving_average_xx_0
  id: blocks_moving_average_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    max_iter: '4096'
    maxoutbuf: '0'
    minoutbuf: '0'
    scale: 1.0/1024.0
    type: float
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [648, 500.0]
    rotation: 0
    state: enabled
- name: blocks_multiply_xx_0
  id: blocks_multiply_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    num_inputs: '2'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [968, 456.0]
    rotation: 0
    state: enabled
- name: blocks_packed_to_unpacked_xx_0
  id: blocks_packed_to_unpacked_xx
  parameters:
    affinity: ''
    alias: ''
    bits_per_chunk: '1'
    comment: ''
    endianness: gr.GR_MSB_FIRST
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1416, 1268.0]
    rotation: 0
    state: true
- name: blocks_pdu_to_tagged_stream_0
  id: blocks_pdu_to_tagged_stream
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    tag: packet_len
    type: byte
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1224, 1276.0]
    rotation: 0
    state: enabled
- name: blocks_rotator_cc_0
  id: blocks_rotator_cc
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    phase_inc: 2.0*math.pi*lo_offset/samp_rate
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1168, 1452.0]
    rotation: 180
    state: true
- name: blocks_socket_pdu_0
  id: blocks_socket_pdu
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    host: 127.0.0.1
    maxoutbuf: '0'
    minoutbuf: '0'
    mtu: '10000'
    port: '16881'
    tcp_no_delay: 'True'
    type: UDP_SERVER
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [282, 1251]
    rotation: 0
    state: enabled
- name: blocks_tagged_stream_multiply_length_0
  id: blocks_tagged_stream_multiply_length
  parameters:
    affinity: ''
    alias: ''
    c: sps_tx * 8
    comment: ''
    lengthtagname: packet_len
    maxoutbuf: '0'
    minoutbuf: '0'
    type: complex
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1856, 1440.0]
    rotation: 180
    state: true
- name: blocks_vco_c_0
  id: blocks_vco_c
  parameters:
    affinity: ''
    alias: ''
    amplitude: '1.0'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    sensitivity: -baudrate*decimation
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [800, 508.0]
    rotation: 0
    state: enabled
- name: bw
  id: parameter
  parameters:
    alias: ''
    comment: 'The bandwidth should configure RF filters on some devices.

      Set to 0.0 for automatic calculation.'
    hide: none
    label: Bandwidth
    short_id: ''
    type: eng_float
    value: '0.0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1120, 12.0]
    rotation: 0
    state: enabled
- name: dc_blocker_xx_0
  id: dc_blocker_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    length: '1024'
    long_form: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    type: ff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1624, 516.0]
    rotation: 0
    state: enabled
- name: decoded_data_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/.satnogs/data/data
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 860.0]
    rotation: 0
    state: enabled
- name: dev_args
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device arguments
    short_id: ''
    type: str
    value: ''
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [520, 12.0]
    rotation: 0
    state: enabled
- name: digital_binary_slicer_fb_0
  id: digital_binary_slicer_fb
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1384, 680.0]
    rotation: 180
    state: true
- name: digital_burst_shaper_xx_0
  id: digital_burst_shaper_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    insert_phasing: 'False'
    length_tag_name: '"packet_len"'
    maxoutbuf: '0'
    minoutbuf: '0'
    post_padding: '100'
    pre_padding: '0'
    type: complex
    window: ([])
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1648, 1420.0]
    rotation: 180
    state: bypassed
- name: digital_chunks_to_symbols_xx_0
  id: digital_chunks_to_symbols_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    dimension: '1'
    in_type: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    num_ports: '1'
    out_type: float
    symbol_table: '[-1, 1]'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1592, 1280.0]
    rotation: 0
    state: true
- name: digital_clock_recovery_mm_xx_0
  id: digital_clock_recovery_mm_xx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    gain_mu: 0.5/8.0
    gain_omega: 2 * math.pi / 100
    maxoutbuf: '0'
    minoutbuf: '0'
    mu: '0.5'
    omega: '2'
    omega_relative_limit: '0.01'
    type: float
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1608, 644.0]
    rotation: 180
    state: enabled
- name: doppler_correction_per_sec
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '20'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [176, 860.0]
    rotation: 0
    state: enabled
- name: enable_iq_dump
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [264, 780.0]
    rotation: 0
    state: enabled
- name: file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: test.wav
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [160, 780.0]
    rotation: 180
    state: enabled
- name: import_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 164.0]
    rotation: 0
    state: enabled
- name: import_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import numpy
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 1524.0]
    rotation: 0
    state: true
- name: import_0_0_0
  id: import
  parameters:
    alias: ''
    comment: ''
    imports: import math
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 1572.0]
    rotation: 0
    state: true
- name: interp_fir_filter_xxx_0
  id: interp_fir_filter_xxx
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    interp: sps_tx
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_delay: '0'
    taps: interp_taps
    type: fff
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1752, 1268.0]
    rotation: 0
    state: enabled
- name: interval_tx
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '1000'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1320, 996.0]
    rotation: 0
    state: enabled
- name: iq_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/iq.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [408, 780.0]
    rotation: 0
    state: enabled
- name: lo_offset
  id: parameter
  parameters:
    alias: ''
    comment: 'To avoid the SDR carrier at the DC

      we shift the LO a little further'
    hide: none
    label: ''
    short_id: ''
    type: eng_float
    value: 100e3
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [808, 13]
    rotation: 0
    state: enabled
- name: low_pass_filter_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: 0.75 * baudrate
    decim: decimation // 2
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1104, 476.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_0_0
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: "Perform a relaxed filter to increase \nthe performance of the auto frequency\n\
      correction"
    cutoff_freq: baudrate*1.25
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: baudrate*decimation
    type: fir_filter_ccf
    width: baudrate / 2.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [280, 476.0]
    rotation: 0
    state: enabled
- name: low_pass_filter_1
  id: low_pass_filter
  parameters:
    affinity: ''
    alias: ''
    beta: '6.76'
    comment: ''
    cutoff_freq: baudrate * 0.60
    decim: '1'
    gain: '1'
    interp: '1'
    maxoutbuf: '0'
    minoutbuf: '0'
    samp_rate: 2 * baudrate
    type: fir_filter_fff
    width: baudrate / 8.0
    win: firdes.WIN_HAMMING
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1440, 476.0]
    rotation: 0
    state: bypassed
- name: pfb_arb_resampler_xxx_0_0_0
  id: pfb_arb_resampler_xxx
  parameters:
    affinity: ''
    alias: ''
    atten: '100'
    comment: ''
    maxoutbuf: '0'
    minoutbuf: '0'
    nfilts: '32'
    rrate: samp_rate/(baudrate*sps_tx)
    samp_delay: '0'
    taps: ''
    type: ccf
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1408, 1428.0]
    rotation: 180
    state: enabled
- name: qtgui_freq_sink_x_0
  id: qtgui_freq_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    average: '1.0'
    axislabels: 'True'
    bw: samp_rate
    color1: '"blue"'
    color10: '"dark blue"'
    color2: '"red"'
    color3: '"green"'
    color4: '"black"'
    color5: '"cyan"'
    color6: '"magenta"'
    color7: '"yellow"'
    color8: '"dark red"'
    color9: '"dark green"'
    comment: ''
    ctrlpanel: 'True'
    fc: rx_freq - lo_offset
    fftsize: '4096'
    freqhalf: 'True'
    grid: 'False'
    gui_hint: ''
    label: Relative Gain
    label1: ''
    label10: ''''''
    label2: ''''''
    label3: ''''''
    label4: ''''''
    label5: ''''''
    label6: ''''''
    label7: ''''''
    label8: ''''''
    label9: ''''''
    legend: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    name: '""'
    nconnections: '1'
    showports: 'False'
    tr_chan: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_tag: '""'
    type: complex
    units: dB
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    wintype: firdes.WIN_BLACKMAN_hARRIS
    ymax: '10'
    ymin: '-140'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 296.0]
    rotation: 0
    state: true
- name: qtgui_time_sink_x_0
  id: qtgui_time_sink_x
  parameters:
    affinity: ''
    alias: ''
    alpha1: '1.0'
    alpha10: '1.0'
    alpha2: '1.0'
    alpha3: '1.0'
    alpha4: '1.0'
    alpha5: '1.0'
    alpha6: '1.0'
    alpha7: '1.0'
    alpha8: '1.0'
    alpha9: '1.0'
    autoscale: 'False'
    axislabels: 'True'
    color1: blue
    color10: dark blue
    color2: red
    color3: green
    color4: black
    color5: cyan
    color6: magenta
    color7: yellow
    color8: dark red
    color9: dark green
    comment: ''
    ctrlpanel: 'False'
    entags: 'True'
    grid: 'False'
    gui_hint: ''
    label1: Signal 1
    label10: Signal 10
    label2: Signal 2
    label3: Signal 3
    label4: Signal 4
    label5: Signal 5
    label6: Signal 6
    label7: Signal 7
    label8: Signal 8
    label9: Signal 9
    legend: 'True'
    marker1: '-1'
    marker10: '-1'
    marker2: '-1'
    marker3: '-1'
    marker4: '-1'
    marker5: '-1'
    marker6: '-1'
    marker7: '-1'
    marker8: '-1'
    marker9: '-1'
    name: '""'
    nconnections: '1'
    size: '1024'
    srate: baudrate
    stemplot: 'False'
    style1: '1'
    style10: '1'
    style2: '1'
    style3: '1'
    style4: '1'
    style5: '1'
    style6: '1'
    style7: '1'
    style8: '1'
    style9: '1'
    tr_chan: '0'
    tr_delay: '0'
    tr_level: '0.0'
    tr_mode: qtgui.TRIG_MODE_FREE
    tr_slope: qtgui.TRIG_SLOPE_POS
    tr_tag: '""'
    type: float
    update_time: '0.10'
    width1: '1'
    width10: '1'
    width2: '1'
    width3: '1'
    width4: '1'
    width5: '1'
    width6: '1'
    width7: '1'
    width8: '1'
    width9: '1'
    ylabel: Amplitude
    ymax: '1'
    ymin: '-1'
    yunit: '""'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1632, 788.0]
    rotation: 0
    state: true
- name: rigctl_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '4532'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [362, 863]
    rotation: 0
    state: enabled
- name: samp_rate
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: Device Sampling rate (common for TX and RX)
    short_id: ''
    type: eng_float
    value: 1e6
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 12.0]
    rotation: 0
    state: enabled
- name: satnogs_crc_async_0
  id: satnogs_crc_async
  parameters:
    affinity: ''
    alias: ''
    check: 'False'
    comment: ''
    crc: satnogs.crc.CRC32_C
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [544, 1268.0]
    rotation: 0
    state: true
- name: satnogs_doppler_compensation_0
  id: satnogs_doppler_compensation
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    compensate: '1'
    fine_correction: '0'
    lo_offset: lo_offset
    maxoutbuf: '0'
    minoutbuf: '0'
    out_samp_rate: baudrate*decimation
    samp_rate: samp_rate
    sat_freq: rx_freq
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [656, 132.0]
    rotation: 0
    state: true
- name: satnogs_frame_decoder_0
  id: satnogs_frame_decoder
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    decoder_object: variable_ieee802_15_4_variant_decoder_0
    itype: byte
    maxoutbuf: '0'
    minoutbuf: '0'
    vlen: '1'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1128, 680.0]
    rotation: 180
    state: true
- name: satnogs_frame_encoder_0
  id: satnogs_frame_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: Contruct the actual frame
    encoder_object: variable_ieee802_15_4_encoder_0
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [1016, 1276.0]
    rotation: 0
    state: true
- name: satnogs_frame_encoder_0_0
  id: satnogs_frame_encoder
  parameters:
    affinity: ''
    alias: ''
    comment: "Re-use the frame encoder block efficiently \nto encode with RS the payload\n\
      and the CRC"
    encoder_object: variable_rs_encoder_0
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [768, 1276.0]
    rotation: 0
    state: true
- name: satnogs_iq_sink_0
  id: satnogs_iq_sink
  parameters:
    activate: enable_iq_dump
    affinity: ''
    alias: ''
    append: 'False'
    comment: ''
    filename: iq_file_path
    scale: '16768'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 508.0]
    rotation: 180
    state: enabled
- name: satnogs_json_converter_0
  id: satnogs_json_converter
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    extra: ''
    maxoutbuf: '0'
    minoutbuf: '0'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [944, 692.0]
    rotation: 180
    state: true
- name: satnogs_multi_format_msg_sink_0
  id: satnogs_multi_format_msg_sink
  parameters:
    affinity: ''
    alias: ''
    comment: ''
    filepath: ''
    format: '0'
    outstream: 'True'
    timestamp: 'False'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [736, 676.0]
    rotation: 180
    state: true
- name: satnogs_udp_msg_sink_0
  id: satnogs_udp_msg_sink
  parameters:
    addr: 127.0.0.1
    affinity: ''
    alias: ''
    comment: ''
    mtu: '1500'
    port: '16880'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [907, 772]
    rotation: 180
    state: true
- name: soapy_rx_device
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: driver=invalid
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [407, 10]
    rotation: 0
    state: enabled
- name: soapy_sink_0
  id: soapy_sink
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: antenna_tx
    ant1: TX
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: '0'
    bw1: '0'
    center_freq0: tx_freq-lo_offset
    center_freq1: 100.0e6
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: soapy_rx_device
    devname: custom
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    iamp_gain0: '0'
    iamp_gain1: '0'
    length_tag_name: ''
    manual_gain0: 'False'
    manual_gain1: 'True'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain_tx
    overall_gain1: '0'
    pad_gain0: '0'
    pad_gain1: '0'
    pga_gain0: gain_tx
    pga_gain1: '0'
    samp_rate: samp_rate
    type: fc32
    vga_gain0: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [944, 1420]
    rotation: 180
    state: enabled
- name: soapy_source_0
  id: soapy_source
  parameters:
    affinity: ''
    alias: ''
    amp_gain0: '0'
    ant0: antenna_rx
    ant1: RX2
    args: dev_args
    balance0: '0'
    balance1: '0'
    bw0: bw
    bw1: '0'
    center_freq0: rx_freq - lo_offset
    center_freq1: '0'
    clock_rate: '0'
    clock_source: ''
    comment: ''
    correction0: '0'
    correction1: '0'
    dc_offset0: '0'
    dc_offset1: '0'
    dc_offset_auto_mode0: 'False'
    dc_offset_auto_mode1: 'False'
    dev: soapy_rx_device
    devname: custom
    gain_auto_mode0: 'False'
    gain_auto_mode1: 'False'
    ifgr_gain: '59'
    lna_gain0: '10'
    lna_gain1: '10'
    manual_gain0: 'True'
    manual_gain1: 'True'
    maxoutbuf: '0'
    minoutbuf: '0'
    mix_gain0: '10'
    mix_gain1: '10'
    nchan: '1'
    nco_freq0: '0'
    nco_freq1: '0'
    overall_gain0: gain_rx
    overall_gain1: '10'
    pga_gain0: '24'
    pga_gain1: '24'
    rfgr_gain: '9'
    samp_rate: samp_rate
    sdrplay_agc_setpoint: '-30'
    sdrplay_biastee: 'True'
    sdrplay_dabnotch: 'False'
    sdrplay_if_mode: Zero-IF
    sdrplay_rfnotch: 'False'
    tia_gain0: '0'
    tia_gain1: '0'
    tuner_gain0: '10'
    tuner_gain1: '10'
    type: fc32
    vga_gain0: '10'
    vga_gain1: '10'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [288, 92.0]
    rotation: 0
    state: true
- name: udp_IP
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: 127.0.0.1
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [512, 780.0]
    rotation: 0
    state: enabled
- name: udp_port
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: intx
    value: '16887'
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [473, 864]
    rotation: 0
    state: enabled
- name: virtual_sink_0
  id: virtual_sink
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [912, 172.0]
    rotation: 0
    state: true
- name: virtual_source_0
  id: virtual_source
  parameters:
    alias: ''
    comment: ''
    stream_id: doppler_corrected
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [40, 452.0]
    rotation: 0
    state: true
- name: waterfall_file_path
  id: parameter
  parameters:
    alias: ''
    comment: ''
    hide: none
    label: ''
    short_id: ''
    type: str
    value: /tmp/waterfall.dat
  states:
    bus_sink: false
    bus_source: false
    bus_structure: null
    coordinate: [16, 780.0]
    rotation: 0
    state: enabled

connections:
- [analog_frequency_modulator_fc_0, '0', blocks_tagged_stream_multiply_length_0, '0']
- [analog_quadrature_demod_cf_0_0, '0', low_pass_filter_1, '0']
- [analog_quadrature_demod_cf_0_0_0_0, '0', blocks_moving_average_xx_0, '0']
- [blocks_delay_0, '0', blocks_multiply_xx_0, '0']
- [blocks_moving_average_xx_0, '0', blocks_vco_c_0, '0']
- [blocks_multiply_xx_0, '0', low_pass_filter_0, '0']
- [blocks_packed_to_unpacked_xx_0, '0', digital_chunks_to_symbols_xx_0, '0']
- [blocks_pdu_to_tagged_stream_0, '0', blocks_packed_to_unpacked_xx_0, '0']
- [blocks_rotator_cc_0, '0', soapy_sink_0, '0']
- [blocks_socket_pdu_0, pdus, satnogs_crc_async_0, pdu]
- [blocks_tagged_stream_multiply_length_0, '0', digital_burst_shaper_xx_0, '0']
- [blocks_vco_c_0, '0', blocks_multiply_xx_0, '1']
- [dc_blocker_xx_0, '0', digital_clock_recovery_mm_xx_0, '0']
- [digital_binary_slicer_fb_0, '0', satnogs_frame_decoder_0, '0']
- [digital_burst_shaper_xx_0, '0', pfb_arb_resampler_xxx_0_0_0, '0']
- [digital_chunks_to_symbols_xx_0, '0', interp_fir_filter_xxx_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', digital_binary_slicer_fb_0, '0']
- [digital_clock_recovery_mm_xx_0, '0', qtgui_time_sink_x_0, '0']
- [interp_fir_filter_xxx_0, '0', analog_frequency_modulator_fc_0, '0']
- [low_pass_filter_0, '0', analog_quadrature_demod_cf_0_0, '0']
- [low_pass_filter_0_0, '0', analog_quadrature_demod_cf_0_0_0_0, '0']
- [low_pass_filter_1, '0', dc_blocker_xx_0, '0']
- [pfb_arb_resampler_xxx_0_0_0, '0', blocks_rotator_cc_0, '0']
- [satnogs_crc_async_0, pdu, satnogs_frame_encoder_0_0, pdu]
- [satnogs_doppler_compensation_0, '0', virtual_sink_0, '0']
- [satnogs_frame_decoder_0, out, satnogs_json_converter_0, in]
- [satnogs_frame_decoder_0, out, satnogs_udp_msg_sink_0, in]
- [satnogs_frame_encoder_0, pdu, blocks_pdu_to_tagged_stream_0, pdus]
- [satnogs_frame_encoder_0_0, pdu, satnogs_frame_encoder_0, pdu]
- [satnogs_json_converter_0, out, satnogs_multi_format_msg_sink_0, in]
- [soapy_source_0, '0', qtgui_freq_sink_x_0, '0']
- [soapy_source_0, '0', satnogs_doppler_compensation_0, '0']
- [virtual_source_0, '0', blocks_delay_0, '0']
- [virtual_source_0, '0', low_pass_filter_0_0, '0']
- [virtual_source_0, '0', satnogs_iq_sink_0, '0']

metadata:
  file_format: 1
