/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FSM_H_
#define FSM_H_

#include "cmsis_os.h"

/**
 * Hold of time step in seconds, must be less than WD timeout
 */
#define POST_DEPLOY_HOLDOFF_TIME_STEP 10

typedef enum {
	FSM_POST_DEPLOY_HOLDOFF,	//!< FSM_POST_DEPLOY_HOLDOFF
	FSM_ANTENNA_DEPLOY,     	//!< FSM_ANTENNA_DEPLOY
	FSM_LOW_POWER_MODE,     	//!< FSM_LOW_POWER_MODE
	FSM_TC_MODE,            	//!< FSM_TC_MODE
	FSM_TELEMETRY_BEACON,   	//!< FSM_TELEMETRY_BEACON
	FSM_EXPERIMENT_MODE     	//!< FSM_EXPERIMENT_MODE
} fsm_state_t;

int
fsm_task();

#endif /* FSM_H_ */
